import { createApp } from 'vue';
import App from './App.vue';
import { myRipple } from '@/components/ripple/my-ripple';

const app = createApp(App);
app.directive('my-ripple', myRipple);
app.mount('#app');

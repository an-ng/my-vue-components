import DataTable from '@/components/data-table/DataTable.vue';
import M3Switch from '@/components/m3-switch/M3Switch.vue';
import M3InputField from '@/components/m3-input-field/M3InputField.vue';
import { myRipple } from '@/components/ripple/my-ripple';
import type { IDataTableHeader } from '@/components/interfaces/IDataTableHeader';

export { DataTable, IDataTableHeader, M3Switch, M3InputField, myRipple };

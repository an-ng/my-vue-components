import type { SortOrder } from '@/components/interfaces/SortOrder';

export type IDataTableHeader = {
  label: string;
  property: string;
  width?: string;
  children?: IDataTableHeader[];
  sort?: boolean;
  sortOrder?: SortOrder;
  compareFn?: (a: unknown, b: unknown) => number;
};

export enum Appearance {
  ELEVATED = 'elevated',
  FILLED = 'filled',
  TONAL = 'tonal',
  OUTLINED = 'outlined',
  TEXT = 'text',
}

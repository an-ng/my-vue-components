import './my-ripple.scss';
import type { Directive, DirectiveBinding, VNode } from 'vue';
import { RippleElement } from '@/components/ripple/ripple-element';

const defaultConfig = {
  enterTransitionDuration: 225,
  leaveTransitionDuration: 150,
};

const calcRectDiagonal = (rippleContainer: HTMLElement): number => {
  return Math.sqrt(
    rippleContainer.clientHeight ** 2 + rippleContainer.clientWidth ** 2
  );
};

const createRippleContainer = (): HTMLDivElement => {
  const container = document.createElement('div');
  container.classList.add('ripple-container');
  return container;
};

const createRippleElement = (e: MouseEvent, radius: number): RippleElement => {
  const rippleEffect = document.createElement('div', {
    is: 'ripple-element',
  }) as RippleElement;

  rippleEffect.classList.add('my-ripple-element');
  rippleEffect.style.width = radius * 2 + 'px';
  rippleEffect.style.height = radius * 2 + 'px';
  rippleEffect.style.top = e.offsetY - radius + 'px';
  rippleEffect.style.left = e.offsetX - radius + 'px';

  return rippleEffect;
};

const onPointerDown = (evt: MouseEvent) => {
  const rippleContainer = evt.target as HTMLDivElement;
  if (!rippleContainer) return;
  const radius = calcRectDiagonal(rippleContainer) / 2;
  const rippleEl = createRippleElement(evt, radius);
  rippleEl.addEventListener('transitionend', (evt) => {
    if (evt.propertyName === 'transform') {
      rippleEl.fadeOut(defaultConfig.leaveTransitionDuration);
    }
    if (evt.propertyName === 'opacity') {
      Promise.resolve(rippleEl).then((el) => {
        rippleContainer.removeChild(el);
      });
    }
  });
  rippleContainer.appendChild(rippleEl);
  rippleEl.fadeIn(defaultConfig.enterTransitionDuration);
};
customElements.define('ripple-element', RippleElement, { extends: 'div' });

export const myRipple: Directive = {
  mounted(
    el: any,
    binding: DirectiveBinding<any>,
    vnode: VNode<any, any>,
    prevVNode: null
  ): void {
    if (binding !== undefined && binding.value === false) return;

    const element = el as HTMLElement;
    const rippleContainer = createRippleContainer();
    element.appendChild(rippleContainer);

    element.style.position = 'relative';
    rippleContainer.addEventListener('mousedown', onPointerDown);
  },
};

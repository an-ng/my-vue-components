export class RippleElement extends HTMLDivElement {
  constructor() {
    super();
    this.classList.add('my-ripple-element');
  }

  fadeIn(transitionDuration: number) {
    this.style.transitionDuration = `${transitionDuration}ms`;
    setTimeout(() => {
      this.style.transform = `scale3d(1, 1, 1)`;
    }, 0);
  }

  fadeOut(transitionDuration: number) {
    this.style.transitionDuration = `${transitionDuration}ms`;
    setTimeout(() => {
      this.style.opacity = `0`;
    }, 0);
  }
}

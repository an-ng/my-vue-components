import DataTable from '@/components/data-table/DataTable.vue';
import type { App, Plugin } from 'vue';

const plugin: Plugin = {
  install(app: App) {
    app.component('DataTable', DataTable);
  },
};

export default plugin;
